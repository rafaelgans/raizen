import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TesteComponent } from './teste/teste';
import { CStepComponent } from './c-step/c-step';

@NgModule({
	declarations: 
            [TesteComponent,
            CStepComponent],
	imports: [],
	exports: 
            [TesteComponent,
            CStepComponent],
        schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ComponentsModule {}
