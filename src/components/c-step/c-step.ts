import { Component } from '@angular/core';

/**
 * Generated class for the CStepComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'c-step',
  templateUrl: 'c-step.html'
})
export class CStepComponent {

  text: string;

  constructor() {
    console.log('Hello CStepComponent Component');
    this.text = 'Hello World';
  }

}
