import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

    isReadyToSave: boolean;
    form: FormGroup;

    constructor(public navCtrl: NavController, public navParams: NavParams, formBuilder: FormBuilder) {
        this.form = formBuilder.group({
          name: ['', Validators.required],
          role: ['', Validators.required],
          field: ['', Validators.required],
          workplace: ['', Validators.required],
          email: ['', Validators.compose([
                      Validators.required,
                      Validators.email
            ])],
          company: ['', Validators.required],
          manager: ['', Validators.required]
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
        });
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad SignupPage');
    }

    doSave(){
        if(this.isReadyToSave) {
            this.navCtrl.push('SignupPermissionPage');
        } else {
            this.isReadyToSave = true;
        }
    }
}
