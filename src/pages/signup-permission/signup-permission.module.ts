import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPermissionPage } from './signup-permission';

@NgModule({
  declarations: [
    SignupPermissionPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupPermissionPage),
  ],
})
export class SignupPermissionPageModule {}
