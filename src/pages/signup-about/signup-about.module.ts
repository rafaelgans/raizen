import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupAboutPage } from './signup-about';

@NgModule({
  declarations: [
    SignupAboutPage
  ],
  imports: [
    IonicPageModule.forChild(SignupAboutPage)
  ],
})
export class SignupAboutPageModule {}
