import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the SignupAboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup-about',
  templateUrl: 'signup-about.html',
})
export class SignupAboutPage {

    isReadyToSave: boolean;
    form: FormGroup;
    inputOpened: false;

    constructor(public navCtrl: NavController, public navParams: NavParams, formBuilder: FormBuilder) {
        this.form = formBuilder.group({
          about: ['', Validators.required],
          accept_tos: ['', Validators.required]
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
        });
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad SignupAboutPage');
    }
    
    doSave(){
        this.navCtrl.push('SignupTagPage');
    }
    
    doNext(){
    }

    doPrevious(){
        this.navCtrl.push('WelcomePage');
    }
}
