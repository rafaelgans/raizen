import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

    isReadyToSave: boolean;
    form: FormGroup;
    step2: false;

    constructor(public navCtrl: NavController, public navParams: NavParams, formBuilder: FormBuilder) {
    
        this.form = formBuilder.group({
          code: ['', Validators.required],
          password: ['', Validators.required]
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad LoginPage');
    }
    
    doLogin(){
        this.navCtrl.push('SignupPage');
    }
}
