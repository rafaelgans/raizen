import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupFieldActivityPage } from './signup-field-activity';

@NgModule({
  declarations: [
    SignupFieldActivityPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupFieldActivityPage),
  ],
})
export class SignupFieldActivityPageModule {}
