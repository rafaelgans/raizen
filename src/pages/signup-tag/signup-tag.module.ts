import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupTagPage } from './signup-tag';

@NgModule({
  declarations: [
    SignupTagPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupTagPage),
  ],
})
export class SignupTagPageModule {}
