import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the SignupTagPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup-tag',
  templateUrl: 'signup-tag.html',
})
export class SignupTagPage {

    isReadyToSave: boolean;
    form: FormGroup;

    constructor(public navCtrl: NavController, public navParams: NavParams, formBuilder: FormBuilder) {
        this.form = formBuilder.group({
          search: ['', Validators.required]
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
        });
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad SignupTagPage');
    }

    doSave(){
        this.doEscape();
    }
    
    doEscape(){
        this.navCtrl.push('SignupFieldWelcomePage');
    }
}
