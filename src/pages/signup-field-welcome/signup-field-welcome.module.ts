import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupFieldWelcomePage } from './signup-field-welcome';

@NgModule({
  declarations: [
    SignupFieldWelcomePage,
  ],
  imports: [
    IonicPageModule.forChild(SignupFieldWelcomePage),
  ],
})
export class SignupFieldWelcomePageModule {}
