import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupFieldPhotoPage } from './signup-field-photo';

@NgModule({
  declarations: [
    SignupFieldPhotoPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupFieldPhotoPage),
  ],
})
export class SignupFieldPhotoPageModule {}
