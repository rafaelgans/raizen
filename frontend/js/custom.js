$(document).ready(function(){
	$(document)
	// exibe label
		.on("focusin","input.hidelabel", function(){
			v = $(this).val();
			where = '#label-' + $(this).attr('id');
			$(where).addClass('opacityFull');
			$(where).removeClass('opacity0');
		})
	// remove label
		.on("focusout","input.hidelabel", function(){
			v = $(this).val();
			where = '#label-' + $(this).attr('id');
			$(where).addClass('opacity0');
			$(where).removeClass('opacityFull');
		})


	// exibe label
		.on("focusin","input.labelhasdata", function(){
			v = $(this).val();
			where = '#label-' + $(this).attr('id');
			$(where).addClass('opacityHalf');
			$(where).removeClass('opacity0');
		})
	// remove label
		.on("focusout","input.labelhasdata", function(){
			v = $(this).val();
			where = '#label-' + $(this).attr('id');
			if(v !== "") {
				$(where).addClass('opacityHalf');
				$(where).removeClass('opacity0');
			} else {
				$(where).removeClass('opacityHalf');
				$(where).addClass('opacity0');
			}
		})


	// cor next step
		.on("keypress","#login", function(){
			$("#goto-step-2").addClass('active');
		})
	// cor submit
		.on("keypress","#senha", function(){
			$("#login-submit").addClass('active');
		})
	// step 1 login
		.on("click","#goto-step-1", function(){
			$(".step-1").show();
			$(".step-2").hide();
		})
	// step 2 login
		.on("click","#goto-step-2", function(){
			$(".step-1").hide();
			$(".step-2").show();
		});


	// form generico
	$('input.labelhasdata').each(function(){
		v = $(this).val();
		where = '#label-' + $(this).attr('id');
		if(v !== "") {
			$(where).addClass('opacityHalf');
			$(where).removeClass('opacity0');
		} else {
			$(where).removeClass('opacityHalf');
			$(where).addClass('opacity0');
		}
	})

});



// verifica se existem dados e exibe a tela correspondente
function loginSteps() {
	if($("#login").val() == "") {
		$("#goto-step-1").click();
		return false;
	} else if($("#senha").val() == "") {
		$("#goto-step-2").click();
		return false;
	} else {
		return true;
	}
}

function splashRedirect() {
	window.location.href = 'login.html';
	location.href = 'login.html';
}


// abre modal pre-definida
function openModal(args) {
	$('.o-'+args).fadeIn();
}
function closeModal() {
	$('.overlay').fadeOut(300);
}



function openFM() {
	$(".full-modal").show();
}
function closeFM() {
	content = $(".moreinfo").val();
	if(content !== '') {
		$(".txt_typed").html( content );
		$(".txt_typed").removeClass('empty');
	}
	$(".full-modal").hide();
}